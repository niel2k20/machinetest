
import { SELECTED_DROPDOWN } from "../actions/types";

 
const INITIAL_STATE={ 
    selectedDropDown:0,
    dropdown : [
        {
            title: 'Godzilla vs. Kong ',
            description: 'The epic next chapter in the cinematic Monsterverse pits two of the greatest icons in motion picture history against one another - the fearsome Godzilla and the mighty Kong - with humanity caught in the balance.'
        },
        {
            title: 'The Little Things',
            description: 'Kern County Deputy Sheriff Joe Deacon is sent to Los Angeles for what should have been a quick evidence-gathering assignment. Instead, he becomes embroiled in the search for a serial killer who is terrorizing the city.'
        },
        {
            title: 'Coolie No. 1',
            description: 'Pandit Jaikishen is insulted by Jeffrey Rosario for not getting a rich matchmaker for daughter Sarah.Jaikishen swears to break ego of Rosario and comes across Raju a Coolie at railway station and asks him to pose as wealthy men to Rosario.Raju and Sarah meet and get married soon and Raju pretends to be thrown out of the house by his father Mahendra Paratap and starts living in a small house with Sarah.WhenRosario comes to meet them he sees that Raju working as a Coolie at the railway station.'
        },
        {
            title: 'Wonder Woman 1984',
            description: 'Diana must contend with a work colleague and businessman, whose desire for extreme wealth sends the world down a path of destruction, after an ancient artifact that grants wishes goes missing.'
        },
        
    ]
}

 export const dropDownReducer = (state = INITIAL_STATE, action)=>{
    console.log('dropdown',action.payload);
    switch (action.type) {
        case SELECTED_DROPDOWN:
            return {
                ...state,
                selectedDropDown : action.payload
            }
        default:
            return state
    }
}


  



