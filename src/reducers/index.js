import { combineReducers } from 'redux';
import { taskReducers } from './taskReducers';
import { authReducer } from './authReducers';
import { dropDownReducer } from './dropDownReducer';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';


const persistConfig = {
    key: 'root',
    storage: storage,
};

const rootReducers = combineReducers({
    tasks: taskReducers,
    auth: authReducer,
    dropdownData: dropDownReducer
})
export default persistReducer(persistConfig, rootReducers)
