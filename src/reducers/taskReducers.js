import { CREATE_TASK,FETCH_TASK, DELETE_TASK } from "../actions/types";

const INITIAL_STATE = {
    tasks: []
}

export const taskReducers = (state = INITIAL_STATE, action) => {

    switch (action.type) {

        case FETCH_TASK:
            return { ...state, tasks: action.payload }
        case DELETE_TASK:
            return {
                ...state,
                tasks: [...state.tasks.slice(0, action.payload),
                ...state.tasks.slice(action.payload + 1)]
            }
        case CREATE_TASK: 
            return {
                ...state,
                tasks: [...state.tasks, action.payload]
            }
        default:
            return state;
    }

}