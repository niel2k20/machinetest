import {SIGN_OUT,SIGN_IN  } from "../actions/types";

const INITIAL_STATE={
    isSignedIn : false,
    userdata : []
}

 export const authReducer = (state = INITIAL_STATE, action)=>{

    switch (action.type) {
        case SIGN_IN:
            return {
                ...state,
                isSignedIn : true,
                userdata : action.payload
            }
        case SIGN_OUT:
            return {
                ...state,
                isSignedIn : false,
                userdata : null
            }
    
        default:
            return state
    }
}