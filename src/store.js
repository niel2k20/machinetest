import { createStore , applyMiddleware, compose  } from "redux";
import { persistStore } from 'redux-persist';
import rootReducers from "./reducers";
import thunk from 'redux-thunk';

 
// const pReducer = persistReducer(persistConfig, rootReducers);
 
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducers,
  composeEnhancers(applyMiddleware(thunk))
  );

const persistor = persistStore(store);
export { persistor, store };
