export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';
export const CREATE_TASK = 'CREATE_TASK';
export const FETCH_TASK = 'FETCH_TASK';
export const SELECTED_DROPDOWN = 'SELECTED_DROPDOWN';
export const DELETE_TASK = 'DELETE_TASK';
