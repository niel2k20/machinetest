import {
    SIGN_IN,
    SIGN_OUT,
    FETCH_TASK,
    DELETE_TASK,
    SELECTED_DROPDOWN,
    CREATE_TASK
} from './types';
import taskApi from '../apis';  


export const signInAndFetchTask = (data) =>{

    return async function(dispatch) {
          await dispatch(trySignIn(data)); 
 
           dispatch(fetchTask()) 
       }
 }

 export const trySignIn = (data) => {
    return {
        type: SIGN_IN,
        payload: data,
    };
};
 

export const trySignOut = () => {
    return {
        type: SIGN_OUT,
    };
};

export const selectDropDown =(index)=>{
    console.log('in index',index);
    return {  
        type : SELECTED_DROPDOWN,
        payload:index
    };
};

export const fetchTask = () => {
    return async function (dispatch) {
        const response = await taskApi.get('/todos'); 
        dispatch({
            type: FETCH_TASK,
            payload: response.data,
        }); 
    };
};


export const createTask = (data) => {
      
     return async function (dispatch) {
         
        await dispatch({
            type: CREATE_TASK,
             payload: data,
        }); 
    };
    
};

export const deleteTask = (id) => { 
    return {
        type: DELETE_TASK,
        payload: id,
    };
};

 





