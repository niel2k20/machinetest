import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Navbar from './components/home/Navbar';
import Home from './components/home/Home';
import Login from './components/auth/Login';
import Task from './components/task/Task';
import AddTask from './components/task/AddTask';
import User from './components/user/User';
import history from "./components/history"; 

function App() {
  return (
    <div className="ui">
      <Router history={history}>
        <Navbar />
        <div>
          <Switch>        
            <Route exact path='/' component={Login} />
            <Route exact path='/home' component={Home} />
            <Route exact path='/user' component={User} />
            <Route exact path='/task'component={Task} />
            <Route exact path='/add'component={AddTask} /> 
          </Switch> 
        </div>
      </Router>
    </div>
  );
}

export default App;
