import React, { Component } from 'react';
import { connect } from 'react-redux'; 
import { createTask } from "../../actions";

class AddTask extends Component {

    state = {
        title: '',
        completed:'false',

    }
    onFormSubmit = async (e) => {
        e.preventDefault();
        const data = {
            userId: 1,
            id: `${this.props.tasks.length + 1}`,
            title: this.state.title,
            completed: this.state.completed
        } 

        if (this.state.title !== '' && this.state.title !== null &this.state.completed !==null) {
            
            await this.props.createTask(data)
        }
        this.props.history.push('/task')
 
    }
    render() {
        return (
            <div>
                <div className='ui segment'>
                    <h2 className='header'>Add Task</h2>
                    <form className="ui form" onSubmit={(e) => this.onFormSubmit(e)}>
                        <div className="field">
                            <label>Title Name</label>
                            <input type="text" name="title" required
                                onChange={(e) => this.setState({ title: e.target.value })}
                                placeholder="ex: john" />
                        </div>
                        <div className="field">
                            <label>Completed Status</label>
                            <div className="ui radio">

                                <input type="radio" checked
                                onChange={(e)=>this.setState({completed:e.target.value})}
                                id="false" name="status" defaultValue="false" />
                                <label htmlFor="false">False</label><br />
                                <input type="radio" id="true" 
                                 onChange={(e)=>this.setState({completed:e.target.value})}
                                name="status" defaultValue="true" />
                                <label htmlFor="true">True</label><br />


                            </div>

                        </div>
                        <button className="ui primary button" type="submit">Submit</button>
                    </form>
                </div> 
                 </div>
        );
    }
}


const mapStateToProps = ({ tasks, auth }) => {
    console.log('List All task Props', tasks.tasks);
    return { 
        tasks: tasks.tasks,
        isSignedIn: auth.isSignedIn
    }
}


export default connect(mapStateToProps, { createTask })(AddTask);