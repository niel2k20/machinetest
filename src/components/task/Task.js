import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchTask,deleteTask,createTask } from "../../actions";
import LoginText from '../auth/LoginText';
class Task extends Component {

    renderList = () => {
        if (this.props.tasks.length <= 0) {
            return(
                <tr>
                    <td>No task Available</td>
                </tr>
            )
        }
        return (
            this.props.tasks.map((items,index) => { 
                return (
                    <tr key={items.id}>
                        <td>{items.id}</td>
                        <td>{items.title}</td>
                        <td>{ String(items.completed).toUpperCase() }</td>
                        <td>
                            <button className="ui red button" onClick={()=>this.props.deleteTask(index)}>
                                Delete
                        </button>

                        </td>
                    </tr>

                )
            })
        )
    }
    addNewTask=()=>{
        this.props.createTask()
    }
    componentDidUpdate=(prevProps,prevState)=>{
console.log('assssssssssss',prevState,prevProps);
    }
    render() {
        if (!this.props.isSignedIn) {
            return(
               <LoginText />
            )
        }
        return (
            <div className='ui segment'>
                
                <Link  to='/add' className="ui primary button">
                    <i className="plus icon"></i>
                    Add Task</Link>
                <table className="ui celled table">
                    <thead>
                        <tr><th>ID</th>
                            <th>Title</th>
                            <th>Completed</th>
                            <th>Actions</th>
                        </tr></thead>
                    <tbody>
                        {this.renderList()}
                         

                    </tbody>
                </table>

                

            </div>
        );
    }
}

const mapStateToProps = ({ tasks,auth }) => {
    console.log('List All task Props', tasks.tasks);
    return { 
        tasks:tasks.tasks,
        isSignedIn:auth.isSignedIn
    }
}

export default connect(mapStateToProps, { fetchTask,deleteTask,createTask })(Task);