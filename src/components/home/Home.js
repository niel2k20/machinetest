import React, { Component } from 'react';
import { connect } from 'react-redux';
import Information from './Information';
import LoginText from '../auth/LoginText';
import DropDown from './DropDown';
class Home extends Component {

 render() {
        if (!this.props.isSignedIn) {
            return (
                <LoginText />
            )
        }
        return (
            <div className='ui segment'>
                <DropDown />
                <Information />


            </div>
        );
    }
}

const mapStateToProps = ({ auth }) => {
    return {
        isSignedIn: auth.isSignedIn
    }
}


export default connect(mapStateToProps)(Home);