import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

class Navbar extends Component {
    render() {
        return (
            <div className="ui menu">
                <div className="header item">
                   <NavLink to='/'>TASK APP</NavLink>
                </div>

                <Link to='/home' className='item'>  Home</Link>
                <Link to='/task' className='item'>  Task</Link>
                <Link to='/user'className='item'>  User</Link>
            </div>

        );
    }
}

export default Navbar;