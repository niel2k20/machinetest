import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectDropDown } from "../../actions";

class DropDown extends Component {
    renderOption=()=>{
        return(
            this.props.dropdownData.map((items,index) => { 
                return (
                    <option key={index} value={index}>{items.title}</option>

                )
            })
        )
    }


    render() {
        return (
            <div className="ui selection">
                    <select className="ui selection dropdown" 
                    onChange={(e)=>this.props.selectDropDown(e.target.value)}>
                        
                        {this.renderOption()} 
                    </select>

                </div>
        );
    }
}

const mapStateToProps = ({ dropdownData }) => { 
    return { 
        dropdownData:dropdownData.dropdown, 
    }
}

export default connect(mapStateToProps,{selectDropDown})(DropDown);