import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signInAndFetchTask } from "../../actions";
class Login extends Component {

    state = {
        username: '',
        password: '',
        isChangePassword: false,
        title: 'Login',


    }

    renderForm = () => {
        return (
            <form className="ui form" onSubmit={(e) => this.onFormSubmit(e)}>
                <div className="field">
                    <label>Username</label>
                    <input type="text" required name="username" placeholder="ex: john"
                        minLength='5' maxLength='10'
                        onChange={(e) => this.setState({ username: e.target.value })} />
                </div>
                <div className="field">
                    <label>Password</label>
                    <input type="password" name="password" required minLength='8' maxLength='10'
                        onChange={(e) => this.setState({ password: e.target.value })}
                        placeholder="***********" />
                </div>
                <button className="ui primary button" type="submit">Submit</button>
            </form>
        )
    }

    onFormSubmit = async (e) => {
        e.preventDefault();
        console.log('ss', this.state.username);
        this.setState({
            path: '/home'
        })
        const data = {
            username: this.state.username,
            password: this.state.password
        }
        if (this.state.username !== '' && this.state.username !== null && this.state.password !== '' && this.state.password !== null) {
            await this.props.signInAndFetchTask(data)
        }
        this.props.history.push('/home')



    }


    render() {
        if (this.props.isSignedIn) {
            this.props.history.push('/home')
        }
        return (
            <div>
                <div className='ui segment'>
                    <h2 className='header'>{this.state.title}</h2>
                    {this.renderForm()}
                </div>



            </div>
        );
    }
}
const mapStateToProps = ({ auth }) => {
    console.log('List All task Props', auth);
    return {

        isSignedIn: auth.isSignedIn
    }
}

export default connect(mapStateToProps, { signInAndFetchTask })(Login);