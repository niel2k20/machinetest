import React, { Component } from 'react';
import { connect } from 'react-redux';
import { trySignOut,trySignIn } from "../../actions";
import LoginText from '../auth/LoginText';

class User extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            isChangePassword: false, 

        }
    }

    renderForm=()=>{
        if (this.state.isChangePassword) {
            return(
                <form className="ui form" onSubmit={(e)=>this.onFormSubmit(e)}>
                <div className='ui grid'>
                    <div className="three wide column"> <h3>Username : </h3> </div>
                    <div className="five wide column"><h3>{this.props.auth.username}</h3></div>
                </div> <br />
                <div className="field">
                <div className='ui grid'>
                    <div className="three wide column"> <h3>Password : </h3> </div>
                    <div className="five wide column"><input type="password" name="password" 
                    onChange={(e)=>this.setState({password:e.target.value})} required minLength='8' maxLength='10'
                    placeholder="***********" /></div>
                </div>
                    
                    
                </div>
                <button className="ui primary button" type="submit">Submit</button>
            </form>
            )
        }
        else{
            return(
                <div className="field">
                <div className='ui grid'>
                    <div className="two wide column"> <h3>Username : </h3> </div>
                    <div className="five wide column"><h3>{this.props.auth.username}</h3></div>
                </div>
                <div className='ui grid'>
                    <div className="two wide column"> <h3>Password : </h3> </div>
                    <div className="five wide column"><h3>*********</h3></div>
                </div>
            </div>
            )
        }
    }


    onFormSubmit = async (e) => {
        e.preventDefault();
        console.log('ss',this.props.auth.username);
        const data = {
            username: this.props.auth.username,
            password: this.state.password
        }
        if (this.state.password!==''&&this.state.password !==null) {
            await this.props.trySignIn(data)
        }

        this.props.history.push('/')
        
    }

    onLogout=async()=>{
        localStorage.clear();
        await this.props.trySignOut()
            this.props.history.push('/')
    }

    render() {
        if (!this.props.isSignedIn) {
            return(
                <LoginText />
            )
        }
        return (
            <div>
                <div className='ui segment'> 
                   {this.renderForm()}
                </div>
                <div className='ui segment'>
                    <button className="ui red button" type="submit" onClick={()=>this.setState({isChangePassword:true})}>Change password</button>
                    <button className="ui teal button" type="submit" onClick={()=>this.onLogout()}>Logout</button>
                </div>


            </div>
        );
    }
}

const mapStateToProps = ({ auth }) => { 
    return { 
        auth: auth.userdata,
        isSignedIn: auth.isSignedIn
    }
}


export default connect(mapStateToProps, { trySignIn,trySignOut })(User);